[HackMD](https://hackmd.io/QbLo8WGdRE6aRK5i8umufw)

# Barcode Backend API

## ItemList
URL: /backend/v1/itemlist

### For admin
以下API需有Admin權限。

#### 1. Create an ItemList
建立一個新的ItemList，同時也會建立該ItemList裡的Items。
> METHOD: POST
> URL: /backend/v1/itemlist/admin
```json=
{
    "name": "Test ItemList 1",
    "items": [
        {
            "name": "item1"
        },
        {
            "name": "item2"
        }
    ],
    "principal": "5db3228ab80c3a3ff19b9508"
}
```

#### 2. Get ItemLists
取得所有ItemLists，可以在req.body設定filter。（不含items細部資料）
> METHOD: GET
> URL: /backend/v1/itemlist/admin

#### 3. Get ItemLists (complete)
取得所有ItemLists，可以在req.body設定filter。（含items細部資料）
> METHOD: GET
> URL: /backend/v1/itemlist/admin/all


#### 4. Admin get an ItemList
讓Admin可以取得任意一個ItemList的Items。
> METHOD: GET
> URL: /backend/v1/itemlist/admin/:id

#### 5. Update ItemList
在URL給定ItemList的id，並將要更新的資料放在req.body。
此方法無法修改、刪除、新增ItemList的Items。
修改Items：Update Item
刪除Items：Delete Item
新增Items到ItemList：使用Add Items to ItemList
> METHOD: PATCH
> URL: /backend/v1/itemlist/admin/:id

#### 6. Delete ItemList
會連帶將ItemList裡的Item刪除。
> METHOD: DELETE
> URL: /backend/v1/itemlist/admin/:id

#### 7. Add Items to ItemList
在itemlist新增items，將新增的Items放在在req.body。
> METHOD: POST
> URL: /backend/v1/itemlist/admin/:id


### For Users
#### 1. Get ItemLists
> METHOD: GET
> URL: /backend/v1/itemlsit/me

#### 2. Get an ItemList
> METHOD: GET
> URL: /backend/v1/itemlist/me/:id

#### 3. Verify ItemList
發送確認ItemList請求。
> METHOD: POST
> URL: /backend/v1/itemlist/me/verify/:id

## Item



