const mongoose = require('mongoose')
const User = require('../../src/model/user')

const AdminId = new mongoose.Types.ObjectId()
const Admin = {
    _id: AdminId,
    name: 'admin@siemens.com',
    isAdmin: true
}

const User1Id = new mongoose.Types.ObjectId()
const User1 = {
    _id: User1Id,
    name: 'user1@siemens.com',
    isAdmin: false
}

const User2Id = new mongoose.Types.ObjectId()
const User2 = {
    _id: User2Id,
    name: 'user2@siemens.com',
    isAdmin: false
}


const setupDatabase = async () => {
    await User.deleteMany()
    await new User(Admin).save()
    await new User(User1).save()
    await new User(user2).save()
}

module.exports = {
    Admin,
    AdminId,
    User1,
    User1Id,
    User2,
    User2Id,
    setupDatabase
}