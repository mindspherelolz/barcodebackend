const  { Router } = require('express')
const BarcodeScanner = require('../model/BarcodeScanner')
const checkAdmin = require('../middleware/checkAdmin')

module.exports = ({ config, db }) => {
    let api = Router();

    // Get all Records
    api.get('/', checkAdmin, async (req, res) => {
        try {
            const data = await BarcodeScanner.find({})
            res.send(data)
        } catch (e) {
            res.status(400).send({ error: e.message })
        }
    })

    // Delete all Records
    api.delete('/', checkAdmin, async (req, res) => {
        try {
            const scannerData = await BarcodeScanner.deleteMany({})
            res.send(scannerData)
        } catch (e) {
            res.status(400).send(e)
        }
    })

    return api;
}