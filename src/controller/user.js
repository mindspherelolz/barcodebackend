const  { Router } = require('express')
const User = require('../model/User')
const checkAdmin = require('../middleware/checkAdmin')


module.exports = ({ config, db }) => {
    let api = Router();

    // This user controlller get data through req.user_name, which is decoded from jwt in getTokenMid middleware.
    api.get('/', async (req, res) => {
        res.send(req.user);
    })
    // Find all users
    api.get('/all', async (req, res) => {
        try {
            const users = await User.find();
            res.send(users);
        } catch (e) {
            res.status(400).send({ error: e.message });
        }
    });

    // Find user by id
    api.get('/id/:id', async (req, res) => {
        try {
            const user = await User.findById(req.params.id);
            if (!user) return res.status(404).send({error: 'User not found.'});
            res.status(200).send(user);
        } catch (e) {
            res.status(400).send({ error: e.message });
        }
    });

    // Find user by email
    api.get('/email/:email', async (req, res) => {
        try {
            const user = await User.findOne({email: req.params.email});
            if (!user) return res.status(404).send({error: 'User not found.'});
            res.status(200).send(user);
        } catch (e) {
            res.status(400).send({ error: e.message });
        }
    });

    // Create an user
    api.post('/', checkAdmin, async (req, res) => {
        try {
            const user = new User(req.body)
            user.save()
            res.status(201).send(user)
        } catch (e) {
            res.status(400).send({ error: e.message })
        }
    });

    // Delete an user by id
    api.delete('/:id', checkAdmin, async (req, res) => {
        try {
            const user = await User.findOne({_id: req.params.id })
            if (!user) return res.status(404).send()
            
            await user.remove()
            res.send(user);
        } catch (e) {
            res.status(400).send({ error: e.message })
        }
    })

    // Update user's own settings
    api.patch('/', async (req, res) => {
        //check if all the updates keys are existed keys in db
        const updates = Object.keys(req.body)
        const allowedUpdates = config.userAllowedUpdates
        const isValidOperation = updates.every((update) => allowedUpdates.includes(update))
        if (!isValidOperation) {
            return res.status(400).send({ error: 'Invalid updates!' })
        }
    
        try {
            await req.user.update(req.body, { new: true })
            res.send(req.user)
        } catch (e) {
            res.status(400).send(e)
        }
    })

    // Admin can update any user settings
    api.patch('/:id', checkAdmin, async (req, res) => {
        //check if all the updates keys are existed keys in db
        const updates = Object.keys(req.body)
        const allowedUpdates = config.userAllowedUpdates
        const isValidOperation = updates.every((update) => allowedUpdates.includes(update))
        if (!isValidOperation) {
            return res.status(400).send({ error: 'Invalid updates!' })
        }

        try {
            const user = await User.findById(req.params.id)
            if (!user) {
                return res.status(404).send()
            }

            updates.forEach((update) => user[update] = req.body[update])
            user.save()
            res.send(user)
        } catch (e) {
            res.status(400).send(e)
        }
    })

    // Admin get all users
    api.get('/admin/all', async (req, res) => {
        const user = req.user
        if (!user){
            res.status(404).send({ error: "Please create your user account via post." })
        } else if (!user.isAdmin) {
            res.status(403).send({ error: "You are not admin." })
        } else {
            res.send(await User.find({}))
        }
    })

<<<<<<< HEAD
=======

>>>>>>> e360dd431e6bf3a3124e089327acbd25594513b2
    return api;
}

