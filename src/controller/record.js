const  { Router } = require('express');
const Record = require('../model/Record');
const checkAdmin = require('../middleware/checkAdmin');

module.exports = ({ config, db }) => {
    let api = Router();

    // Get all Records
    api.get('/', async (req, res) => {
        try {
            const records = await Record.find()
            res.send(records)
        } catch (e) {
            res.status(400).send({ error: e.message })
        }
    });

    // Delete  Record by id
    api.delete('/:id', checkAdmin, async (req, res) => {
        try {
            const record = await Record.findOne({_id:req.params.id});
            if (!record) return res.status(404).send();

            await record.remove();
            res.send(record);
        } catch (e) {
            res.status(400).send({ error: e.message });
        }
    })

    return api;
}