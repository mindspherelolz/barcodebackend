const  { Router } = require('express')
const ItemList = require('../model/ItemList')
const ScanList = require('../model/ScanList')
const Item = require('../model/Item')
const Record = require('../model/Record')
const checkAdmin = require('../middleware/checkAdmin')
const async = require('async')

module.exports = ({ config, db }) => {
    let api = Router();

    // Get ItemLists
    api.get('/', async (req, res) => {
        res.send(await ItemList.find())
    });

    // Get an ItemList by id
    api.get('/:id', async (req, res) => {
        const itemList = await ItemList.findOne({ _id: req.params.id })
        if(!itemList) return res.status(404).send()

        if (req.user.isAdmin || itemList.operator.toString() == req.user._id) {
            res.send(itemList);
        } else {
            res.status(403).send();
        }
    })

    // Get Items of an ItemList by id
    api.get('/:id/items', async (req, res) => {
        await ScanList.findOne({ itemList: req.params.id }, function (err, list) {
            let itemsDetailList = [];
            async.each(list.items,function(target,callback){
                if(target.demand > 0 ){
                    Item.findById(target._id,function(err,item){
                            let itemDetail = {};
                            itemDetail.item = item;
                            itemDetail.quantity = target.demand;
                            itemDetail.scaned = target.scaned;
                            itemsDetailList.push(itemDetail);
                            callback();
                    });
                }else{
                    callback();
                }
            },function(err){
                if(!err){
                    return res.send(itemsDetailList);
                }else{
                    return res.status(404).send({err: err});
                }
            });
        });
    })
    

    //Create an ItemList
    api.post('/', checkAdmin, async (req, res) => {
        try {
            const newItemList = await new ItemList(req.body);
            const newScanList = await new ScanList();
            newScanList.itemList = newItemList._id;
            newItemList.scanList = newScanList._id;
            newItemList.items.forEach(item => {
                newScanList.items.push({
                    item: item.item,
                    demand: item.quantity
                });
            });
            await newItemList.save();
            await newScanList.save();
            res.status(201).send(newItemList);
        } catch (e) {
            // res.status(400).send({ error: 'Bad request.'})
            res.status(400).send({ error: e.message });
        }
    })



    // Update an ItemList by id
    api.patch('/:id', checkAdmin, async (req, res) => {
        // First, check if there is any invalid update key
        const updates = Object.keys(req.body)
        const allowedUpdates = config.itemListAllowedUpdates
        const isValidOperation = updates.every((update) => allowedUpdates.includes(update))
        if (!isValidOperation) {
            return res.status(400).send({ error: 'Invalid updates!' })
        }

        try {
            const itemList = await ItemList.findOne({ _id: req.params.id })
            if (!itemList) {
                return res.status(404).send()
            }
            updates.forEach((update) => itemList[update] = req.body[update])
            itemList.save()
            res.send(itemList)
        } catch (e) {
            res.status(400).send({ error: 'Bad request.', e: e.message})
        }
        
    })

    // Delete an ItemList by id
    api.delete('/:id', checkAdmin, async (req, res) => {
        try {
            const itemList = await ItemList.findOne({ _id: req.params.id })
            const scanList = await ScanList.findOne({ itemList: req.params.id })
            if (!itemList || !scanList) return res.status(404).send()
            await itemList.remove()
            await scanList.remove()
            res.status(200).send(itemList)
        } catch (e) {
            res.status(400).send({ error: e.message })
        }
    })


    // ------- below is not REST API -------
    // Two implementation:
    // 1. Scan an Item via web, instead of MSP asset
    // 2. Submit an ItemList


    // Scan item
    api.post('/scanItem/:id', async(req,res) => {
        try{
            const itemList = await ItemList.findOne({ _id: req.params.id });
            const scanList = await ScanList.findOne({ itemList: req.params.id });
            if (!itemList) return res.status(404).send('Item list is not found.');

            // Step 1: Get the scanned Items
            if(req.body){
                const item = await Item.findById(req.body.id);
                scanedItems = {
                    item: item._id,
                    scaned: req.body.quantity,
                    wrong: true
                }
                
                // Save to DB
                var found = false;
                var index = -1;
                for (var i = 0; i < scanList.items.length; i++) {
                    if (JSON.stringify(scanList.items[i]._id) == JSON.stringify(scanedItem._id)) {
                        index = i;
                        break;
                    }
                }

                if(index != -1){
                    const target = scanList.items[index]
                    target.scaned += scanedItem.scaned
                    if (target.demand > 0) {
                        scanedItem.wrong = false
                        if (target.scaned > target.demand) {
                            scanedItem.wrong = true
                        }
                    }

                    target.wrong = scanedItem.wrong
                } else {
                    scanList.items.push(scannedItem)
                }

                var unfilled = scanList.items.find(item => {
                    return item.scaned < item.demand;
                });
                
                if(!unfilled){
                    itemList.fulfilled = true;
                }

                await itemList.save();
                await scanList.save();
                res.status(200).send(itemList);
            }else{
                res.status(404).send({ error: 'Give me a body plz.'});
            }

        }catch(e){
            res.status(400).send({ error: 'Scan Error' , e: e.message});
        }
    });

    // Submit item list
     api.post('/submit/:id', async(req,res) => {
        try{    
            const itemList = await ItemList.findOne({ _id: req.params.id });
            const scanList = await ScanList.findOne({ itemList: req.params.id });
            if (itemList.submitted) return res.status(400).send({error: 'Already submitted.'});
            itemList.submitted = true;
            await itemList.save();

            const newRecord = await new Record();
            newRecord.result = itemList.fulfilled;
            newRecord.itemList = itemList._id;
            newRecord.scanList = scanList._id;
            await newRecord.save();
            
            res.status(200).send(newRecord);
         }catch(e){
            res.status(400).send({ error: 'Submit Error' , e: e.message});
         }
     });

    return api;
}