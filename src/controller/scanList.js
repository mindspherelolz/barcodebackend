const  { Router } = require('express');
const ScanList = require('../model/ScanList');
const Item = require('../model/Item');
const checkAdmin = require('../middleware/checkAdmin');
const async = require('async');

module.exports = ({ config, db }) => {
    let api = Router();

    //GET - get all scan list.
    api.get('/', async (req, res) => {
        try {
            const scanList = await ScanList.find()
            res.send(scanList)
        } catch (e) {
            res.status(400).send({ error: e.message })
        }
    });

    //GET - get a scan list by scan id.
    api.get('/:id', async (req, res) => {
        try {
            const scanList = await ScanList.findById(req.params.id);
            res.send(scanList)
        } catch (e) {
            res.status(400).send({ error: e.message })
        }
    });

    //GET - get a scan list by item list id.
    api.get('/itemList/:id', async (req, res) => {
        try {
            const scanList = await ScanList.findOne({ itemList: req.params.id });
            res.send(scanList);
        } catch (e) {
            res.status(400).send({ error: e.message })
        }
    });

    //GET - get wrong scan list by item list id.
    api.get('/wrong/:id', async (req, res) => {
        try {
            await ScanList.findOne({ itemList: req.params.id }, function (err, list) {
                let wrongList = [];
                async.each(list.items,function(target,callback){
                    if(target.wrong){
                        Item.find(target._id,function(err,item){
                            wrongList.push({item,scaned: target.scaned});
                            callback();
                        });
                    }else{
                        callback();
                    }
                },function(err){
                    if(!err){
                        return res.send(wrongList);
                    }else {
                        return res.status(404).send({ error: e.message });
                    }
                });
            });
        } catch (e) {
            res.status(400).send({ error: e.message })
        }
    });

    return api;
}
