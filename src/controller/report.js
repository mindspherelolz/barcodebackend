const  { Router } = require('express');
const Report = require('../model/Report');
const checkAdmin = require('../middleware/checkAdmin');

module.exports = ({ config, db }) => {
    let api = Router();

    // Get all Reports
    api.get('/', async (req, res) => {
        try {
            const reports = await Report.find()
            res.send(reports)
        } catch (e) {
            res.status(400).send({ error: e.message })
        }
    });

    // Get Maintenance Reports
    api.get('/maintenance', async (req, res) => {
        try {
            const reports = await Report.find({ people: "維修員" });
            res.send(reports)
        } catch (e) {
            res.status(400).send({ error: e.message })
        }
    });

    // Get Admin Reports
    api.get('/admin', async (req, res) => {
        try {
            const reports = await Report.find({ people: "管理員" });
            res.send(reports)
        } catch (e) {
            res.status(400).send({ error: e.message })
        }
    });

    // Get Site Reports
    api.get('/site', async (req, res) => {
        try {
            const reports = await Report.find({ people: "現場人員" });
            res.send(reports)
        } catch (e) {
            res.status(400).send({ error: e.message })
        }
    });

    // Get Report by id
    api.get('/:id', async (req, res) => {
        try {
            const report = await Report.findById(req.params.id);
            res.send(report)
        } catch (e) {
            res.status(400).send({ error: e.message })
        }
    });

    // Create Report
    api.post('/', async (req,res) => {
        try{
            const report = new Report(req.body);
            report.save();
            res.status(200).send(report);
        }catch(e){
            res.status(400).send({ error: e.message });
        }
    });

    // Delete Report by id
    api.delete('/:id', checkAdmin, async (req, res) => {
        try {
            const report = await Report.findOne({_id:req.params.id});
            if (!report) return res.status(404).send();

            await report.remove();
            res.send(report);
        } catch (e) {
            res.status(400).send({ error: e.message });
        }
    });

    // Update Report by id
    api.put('/:id', checkAdmin, async(req, res) => {
        try {
            const report = await Report.findById(req.params.id)
            if (!report) {
                return res.status(404).send()
            }

            report.save()
            res.send(report)
        } catch (e) {
            res.status(400).send(e)
        }
    });

    return api;
}