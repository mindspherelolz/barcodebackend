const  { Router } = require('express')
const Item = require('../model/Item')
const checkAdmin = require('../middleware/checkAdmin')


module.exports = ({ config, db }) => {
    let api = Router();

    // Gets all items
    api.get('/', async (req, res) => {
        try {
            const items = await Item.find()
            res.send(items)
        } catch (e) {
            res.status(400).send({ error: e.message })
        }
    })

    // Get an item by id
    api.get('/:id', async (req, res) => {
        try {
            const item = await Item.findById(req.params.id)
            if (!item) return res.status(404).send()
            res.send(item)
        } catch (e) {
            res.status(400).send({ error: e.message })
        }
    })

    // Create an item
    api.post('/', checkAdmin, async (req, res) => {
        try {
            const item = new Item(req.body)
            item.save()
            res.status(201).send(item)
        } catch (e) {
            res.status(400).send({ error: e.message })
        }
    })

    // Update an item by id
    api.patch('/:id', checkAdmin, async (req, res) => {
        try {
            const updates = Object.keys(req.body)
            const allowedUpdates = config.itemAllowedUpdates
            const isValidOperation = updates.every((update) => allowedUpdates.includes(update))
            if (!isValidOperation) {
                return res.status(400).send({ error: 'Invalid updates!' })
            }

            const item = await Item.findByIdAndUpdate(req.params.id, req.body, { new: true })
            if (!item) return res.status(404).send()
            res.send(item)
        } catch (e) {
            res.status(400).send({ error: e.message })
        }
    })

    // Update: Increment the quantity of an item
    api.patch('/quantity/add/:id&:num', checkAdmin, async (req, res) => {
        try {
            const item = await Item.findById(req.params.id)
            if (!item) return res.status(404).send()

            item['quantity'] = item['quantity'] + parseInt(req.params.num)
            item.save()
            res.send(item)
        } catch (e) {
            res.status(400).send({ error: e.message })
        }
    })

    // Delete an item by id
    api.delete('/:id', checkAdmin, async (req, res) => {
        try {

            const item = await Item.findOne({_id: req.params.id })
            if (!item) return res.status(404).send()
            
            await item.remove()
            res.send(item)
        } catch (e) {
            res.status(400).send({ error: e.message })
        }
    })

    return api;
}
