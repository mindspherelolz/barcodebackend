const { Router } = require('express')
const httpRequest = require('request')
const { mspURL } = require('./mspConfig')


let api = Router();

    // '/msp/assettypes/' - Read
    api.get('/assettypes', (req, res) => {
        httpRequest.get({
            url: mspURL+'/assettypes',
            headers: { 'authorization': 'Bearer ' + req.token, 'content-type': 'application/json' },
            qs: req.query
        }, function (error, answer, body) {
            if (!error && answer.statusCode == 204 || 200 ) {
                res.status(200).json(JSON.parse(body)._embedded.assetTypes);
            } else {
                res.status(400).send(error.message)
            }
        });
    });

    api.get('/aspecttypes', (req, res) => {
        httpRequest.get({
            url: mspURL+'/aspecttypes',
            headers: { 'authorization': 'Bearer ' + req.token, 'content-type': 'application/json' },
            qs: req.query
        }, function (error, answer, body) {
            if (!error && answer.statusCode == 204 || 200 ) {
                res.status(200).json(JSON.parse(body)._embedded.aspectTypes);
            } else {
                res.status(400).send(error.message)
            }
        });
    });

    // Create or update aspecttypes
    api.put('/aspecttypes/:name', (req, res) => {
        httpRequest.put({
            url: mspAssetURL+'/aspecttypes/'+req.params.name,
            headers: { 'authorization': 'Bearer ' + req.token, 'content-type': 'application/json', 'If-Match': 1 },
            body: req.body
        }, function (error, answer, body) {
            if (!error && answer.statusCode == 200) {
                res.status(200).json(JSON.parse(body));
            } else {
                res.status(400).send(error.message)
            }
        });
    });

    // Create or update assettypes
    api.put('/assettypes/:name', (req, res) => {
        httpRequest.put({
            url: mspAssetURL+'/assettypes/'+req.params.name,
            headers: { 'authorization': 'Bearer ' + req.token, 'content-type': 'application/json', 'If-Match': 1 },
            body: req.body
        }, function (error, answer, body) {
            if (!error && answer.statusCode == 200) {
                res.status(200).json(JSON.parse(body));
            } else {
                res.status(400).send(error.message)
            }
        });
    });

    // Delete assettypes
    api.delete('/assettypes/:name', (req, res) => {
        httpRequest.delete({
            url: mspAssetURL+'/assettypes/'+req.params.name,
            headers: { 'authorization': 'Bearer ' + req.token, 'content-type': 'application/json', 'If-Match': 1 },
        }, function (error, answer, body) {
            if (!error && answer.statusCode == 200 || 204) {
                res.status(200).json(JSON.parse(body));
            } else {
                res.status(400).send(error.message)
            }
        });
    });

    // Delete aspecttypes
    api.delete('/aspecttypes/:name', (req, res) => {
        httpRequest.delete({
            url: mspAssetURL+'/aspecttypes/'+req.params.name,
            headers: { 'authorization': 'Bearer ' + req.token, 'content-type': 'application/json', 'If-Match': 1 },
        }, function (error, answer, body) {
            if (!error && answer.statusCode == 200 || 204) {
                res.status(200).json(JSON.parse(body));
            } else {
                res.status(400).send(error.message)
            }
        });
    });

module.exports = api
