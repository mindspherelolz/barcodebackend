const { Router } = require('express')
const httpRequest = require('request')
const { mspEventURL } = require('./mspConfig')


let api = Router();

    // '/msp/events/' - Read
    api.get('/', (req, res) => {
        if(!req.token) return res.status(401).send('Acess denied. No token provided.');
        httpRequest.get({
            url: mspEventURL,
            headers: { 'authorization': 'Bearer ' + req.token, 'content-type': 'application/json' },
            qs: req.query
        }, function (error, answer, body) {
            if (!error && answer.statusCode == 204 || 200 ) {
                if (JSON.parse(body)._embedded === undefined) res.status(200).send([])
                else res.status(200).json(JSON.parse(body)._embedded.events);
            } else {
                res.status(400).send(error.message)
            }
        });
    });

    // Get event details
    api.get('/:id', (req, res) => {
        if(!req.token) return res.status(401).send('Acess denied. No token provided.');
        httpRequest.get({
            url: mspEventURL + `?filter={"entityId": "${req.params.id}"}`,
            headers: { 'authorization': 'Bearer ' + req.token, 'content-type': 'application/json' },
        }, function (error, answer, body) {
            if (!error && answer.statusCode == 204 || 200 ) {
                if (JSON.parse(body)._embedded === undefined) res.status(200).send([])
                else res.status(200).json(JSON.parse(body)._embedded.events);
            } else {
                res.status(400).send(error.message)
            }
        });
    });

    // Create event
     api.post('/', (req, res) => {
        if(!req.token) return res.status(401).send('Acess denied. No token provided.');
        httpRequest.post({
            url: mspEventURL,
            headers: { 'authorization': 'Bearer ' + req.token, 'content-type': 'application/json', 'Accept': '*/*' },
            json: req.body
        }, function (error, answer, body) {
            if (!error && (answer.statusCode == 204 || 200 || 201)) {
                res.status(201).json(body);
            } else {
                res.status(400).send(error.message)
            }
        });
    });

    // Bulk delete event
    api.post('/deleteEventsJobs', (req, res) => {
        if(!req.token) return res.status(401).send('Acess denied. No token provided.');
        httpRequest.post({
            url: mspEventURL+'deleteEventsJobs',
            headers: { 'authorization': 'Bearer ' + req.token, 'content-type': 'application/json' },
            body: req.body
        }, function (error, answer, body) {
            if (!error && answer.statusCode == 200 || 204 ) {
                res.status(200).json(body);
            } else {
                res.status(400).send(error.message)
            }
        });
    });

    // Bulk create event
    api.post('/createEventsJobs', (req, res) => {
        if(!req.token) return res.status(401).send('Acess denied. No token provided.');
        httpRequest.post({
            url: mspEventURL+'createEventsJobs',
            headers: { 'authorization': 'Bearer ' + req.token, 'content-type': 'application/json' },
            body: req.body
        }, function (error, answer, body) {
            if (!error && answer.statusCode == 200 || 204 ) {
                res.status(200).json(body);
            } else {
                res.status(400).send(error.message)
            }
        });
    });

    // Bulk delete getinfo
    api.delete('/deleteEventsJobs/:id', (req, res) => {
        if(!req.token) return res.status(401).send('Acess denied. No token provided.');
        var url = `https://gateway.eu1.mindsphere.io/api/eventmanagement/v3/deleteEventsJobs`;
        var filter = {
            "filter": {
              "typeId": "com.siemens.mindsphere.eventmgmt.event.type.MindSphereStandardEvent",
              "id": req.params.id
            }
          }
        httpRequest.post({
            // url: mspEventURL+'deleteEventsJobs/',
            url: url,
            headers: { 'authorization': 'Bearer ' + req.token, 'content-type': 'application/json', 'Accept': '*/*', 'If-Match' : 0 },
            json: filter
        }, function (error, answer, body) {
            if (!error && answer.statusCode == 200 || 204 ) {
                res.status(200).json(body);
            } else {
                res.status(400).send(error.message)
            }
        });
    });

    // Bulk create getinfo
    api.get('/createEventsJobs/:id', (req, res) => {
        if(!req.token) return res.status(401).send('Acess denied. No token provided.');
        httpRequest.get({
            url: mspEventURL+'createEventsJobs/'+req.params.id,
            headers: { 'authorization': 'Bearer ' + req.token, 'content-type': 'application/json' }
        }, function (error, answer, body) {
            if (!error && answer.statusCode == 200 || 204 ) {
                res.status(200).json(body);
            } else {
                res.status(400).send(error.message)
            }
        });
    });

    // Update event
    api.put('/:id', (req, res) => {
        if(!req.token) return res.status(401).send('Acess denied. No token provided.');
        httpRequest.put({
            url: mspEventURL+req.params.id,
            headers: { 'authorization': 'Bearer ' + req.token, 'content-type': 'application/json', 'If-Match': req.headers["if-match"] },
            body: req.body
        }, function (error, answer, body) {
            if (!error && answer.statusCode == 200 || 204 ) {
                res.status(200).json(body);
            } else {
                res.status(400).send(error.message)
            }
        });
    });

    // Update event
    api.put('/:id', (req, res) => {
        if(!req.token) return res.status(401).send('Acess denied. No token provided.');
        httpRequest.put({
            url: mspEventURL+req.params.id,
            headers: { 'authorization': 'Bearer ' + req.token, 'content-type': 'application/json', 'If-Match': req.headers["if-match"] },
            body: req.body
        }, function (error, answer, body) {
            if (!error && answer.statusCode == 200 || 204 ) {
                res.status(200).json(body);
            } else {
                res.status(400).send(error.message)
            }
        });
    });


module.exports = api
