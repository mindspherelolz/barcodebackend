const { Router } = require('express')
const httpRequest = require('request')
const { mspURL } = require('./mspConfig')

let api = Router();

api.get('*', (req, res) => {
    httpRequest.get({
        url: mspURL+req.url,
        headers: { 'authorization': 'Bearer ' + req.token, 'content-type': 'application/json', 'If-Match': req.headers["if-match"] },
        qs: req.query,
        body: req.body
    }, function (error, answer, body) {
        if (!error && [200,201,204].includes(answer.statusCode)) {
            res.status(answer.statusCode).json(JSON.parse(body))
        } else {
            res.status(400).send('Bad request or mindsphere API service is down.')
        }
    });
});

api.post('*', (req, res) => {
    httpRequest.post({
        url: mspURL+req.url,
        headers: { 'authorization': 'Bearer ' + req.token, 'content-type': 'application/json', 'If-Match': req.headers["if-match"] },
        qs: req.query,
        body: req.body
    }, function (error, answer, body) {
        if (!error && [200,201,204].includes(answer.statusCode)) {
            res.status(answer.statusCode).json(JSON.parse(body))
        } else {
            res.status(400).send('Bad request or mindsphere API service is down.')
        }
    });
});

api.put('*', (req, res) => {
    httpRequest.put({
        url: mspURL+req.url,
        headers: { 'authorization': 'Bearer ' + req.token, 'content-type': 'application/json', 'If-Match': req.headers["if-match"] },
        qs: req.query,
        body: req.body
    }, function (error, answer, body) {
        if (!error && [200,201,204].includes(answer.statusCode)) {
            res.status(answer.statusCode).json(JSON.parse(body))
        } else {
            res.status(400).send('Bad request or mindsphere API service is down.')
        }
    });
});

api.delete('*', (req, res) => {
    httpRequest.delete({
        url: mspURL+req.url,
        headers: { 'authorization': 'Bearer ' + req.token, 'content-type': 'application/json', 'If-Match': req.headers["if-match"] },
        qs: req.query,
        body: req.body
    }, function (error, answer, body) {
        if (!error && [200,201,204].includes(answer.statusCode)) {
            res.status(answer.statusCode).json(JSON.parse(body))
        } else {
            res.status(400).send('Bad request or mindsphere API service is down.')
        }
    });
});

api.patch('*', (req, res) => {
    httpRequest.patch({
        url: mspURL+req.url,
        headers: { 'authorization': 'Bearer ' + req.token, 'content-type': 'application/json', 'If-Match': req.headers["if-match"] },
        qs: req.query,
        body: req.body
    }, function (error, answer, body) {
        if (!error && [200,201,204].includes(answer.statusCode)) {
            res.status(answer.statusCode).json(JSON.parse(body))
        } else {
            res.status(400).send('Bad request or mindsphere API service is down.')
        }
    });
});


module.exports = api
