const { Router } = require('express');
const jwtDecode = require('jwt-decode');

let api = Router();

    //Get - MindSphere token
    api.get('/', async (req, res) => {
        try{
            res.status(200).json({ token: req.token });
        }catch(e){
            res.status(404).json({ err: e });
        }
    });

    //Get - MindSphere decoded token
    api.get('/decode', async(req, res) => {
        try{
            let token = req.token;
            let decoded = jwtDecode(token);
            res.status(200).json(decoded);
        }catch(e){
            res.status(404).json({ err: e });
        }
    });

    //Get - MindSphere decoded token
    api.get('/user', async(req, res) => {
        try{
            const token = req.token;
            let decoded = jwtDecode(token);
            var admin = decoded.scope.some(element => {
                return element == "barcode.admin";
            });
            let user = {
                _id: decoded.user_id,
                name: decoded.user_name,
                email: decoded.email,
                admin: admin
            }
            res.status(200).json(user);
        }catch(e){
            res.status(404).json({ err: e });
        }
    });
module.exports = api;