const { Router } = require('express')
const httpRequest = require('request')
const { mspAgentURL } = require('./mspConfig')

let api = Router();

// Get agents
api.get('/', (req, res) => {
    httpRequest.get({
        url: mspAgentURL,
        headers: { 'authorization': 'Bearer ' + req.token, 'content-type': 'application/json' },
        qs: req.query
    }, function (error, answer, body) {
        if (!error && answer.statusCode == 204 || 200 ) {
            res.status(200).json(JSON.parse(body));
        } else {
            res.status(400).send(error.message)
        }
    });
});

// Get agent datasources
api.get('/:id/dataSourceConfiguration', (req, res) => {
    httpRequest.get({
        url: mspAgentURL+req.params.id+'/dataSourceConfiguration',
        headers: { 'authorization': 'Bearer ' + req.token, 'content-type': 'application/json' },
        qs: req.query
    }, function (error, answer, body) {
        if (!error && answer.statusCode == 204 || 200 ) {
            res.status(200).json(JSON.parse(body));
        } else {
            res.status(400).send(error.message)
        }
    });
});

// Delete agent
api.delete('/:id', (req, res) => {
    httpRequest.delete({
        url: mspAgentURL+req.params.id,
        headers: { 'authorization': 'Bearer ' + req.token, 'content-type': 'application/json', 'If-Match': req.headers["if-match"] },
    }, function (error, answer, body) {
        if (!error && answer.statusCode == 204 || 200 ) {
            res.status(200).json(JSON.parse(body));
        } else {
            res.status(400).send(error.message)
        }
    });
});



module.exports = api
