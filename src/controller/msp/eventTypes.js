const { Router } = require('express')
const httpRequest = require('request')
const { mspEventTypeURL } = require('./mspConfig')

let api = Router();

    // Get eventTypes
    api.get('/', (req, res) => {
        httpRequest.get({
            url: mspEventTypeURL,
            headers: { 'authorization': 'Bearer ' + req.token, 'content-type': 'application/json' },
            qs: req.query
        }, function (error, answer, body) {
            if (!error && answer.statusCode == 204 || 200 ) {
                if (JSON.parse(body)._embedded === undefined) res.status(200).send([])
                else res.status(200).json(JSON.parse(body)._embedded.eventTypes);
            } else {
                res.status(400).send(error.message)
            }
        });
    });

    // Get eventTypes
    api.post('/', (req, res) => {
        httpRequest.post({
            url: mspEventTypeURL,
            headers: { 'authorization': 'Bearer ' + req.token, 'content-type': 'application/json' },
            body: req.body
        }, function (error, answer, body) {
            if (!error && answer.statusCode == 204 || 200 ) {
                res.status(200).json(JSON.parse(body));
            } else {
                res.status(400).send(error.message)
            }
        });
    });


module.exports = api