const { Router } = require('express')
const httpRequest = require('request')
const { mspFileURL } = require('./mspConfig')
const multer = require('multer');
const storage = multer.memoryStorage()
const upload = multer({ storage: storage })

let api = Router();

    // '/msp/file/' - POST
    api.post('/:assetId/:name', upload.single('image'), (req, res) => {
        const image = req.file.buffer
        if(!req.token) return res.status(401).send('Acess denied. No token provided.');
        httpRequest.put({
            url: mspFileURL + `${req.params.assetId}/${req.params.name}`,
            headers: { 'authorization': 'Bearer ' + req.token, 'content-type': 'application/octet-stream', 'type': 'image/*' },
            json: image
        }, function (error, answer, body) {
            if (!error && answer.statusCode == 204 || 200 || 201) {
                res.status(200).json(body);
            } else {
                res.status(400).send(error.message);
            }
        });
    });


module.exports = api
