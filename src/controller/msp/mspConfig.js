const mspURL = 'https://gateway.eu1.mindsphere.io/api'

module.exports =  {
    mspURL,
    mspAssetURL: mspURL+'/assetmanagement/v3/assets/',
    mspEventURL: mspURL+'/eventmanagement/v3/events/',
    mspEventTypeURL: mspURL+'/eventmanagement/v3/eventTypes/',
    mspAgentURL: mspURL+'/agentmanagement/v3/agents/',
    mspTimeseriesURL: mspURL+'/iottimeseries/v3/timeseries/',
    mspAggregateURL: mspURL+'/iottsaggregates/v3/aggregates/',
    mspFileURL: mspURL+'/iotfile/v3/files/'
}