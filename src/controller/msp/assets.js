const { Router } = require('express')
const httpRequest = require('request')
const { mspAssetURL } = require('./mspConfig')


let api = Router();

    // '/msp/assets/' - Read
    api.get('/', (req, res) => {
        if(!req.token) return res.status(401).send('Acess denied. No token provided.');
        httpRequest.get({
            url: mspAssetURL,
            headers: { 'authorization': 'Bearer ' + req.token, 'content-type': 'application/json' },
            qs: req.query
        }, function (error, answer, body) {
            if (!error && answer.statusCode == 204 || 200 ) {
                res.status(200).json(JSON.parse(body)._embedded.assets);
            } else {
                res.status(400).send(error.message)
            }
        });
    });

    // Get asset details
    api.get('/:id', (req, res) => {
        httpRequest.get({
            url: mspAssetURL+req.params.id,
            headers: { 'authorization': 'Bearer ' + req.token, 'content-type': 'application/json' }
        }, function (error, answer, body) {
            if (!error && answer.statusCode == 204 || 200 ) {
                res.status(200).json(JSON.parse(body));
            } else {
                res.status(400).send(error.message)
            }
        });
    });

    // Get asset aspects
    api.get('/:id/aspects', (req, res) => {
        httpRequest.get({
            url: mspAssetURL+req.params.id+'/aspects',
            headers: { 'authorization': 'Bearer ' + req.token, 'content-type': 'application/json' }
        }, function (error, answer, body) {
            if (!error && answer.statusCode == 204 || 200 ) {
                res.status(200).json(JSON.parse(body)._embedded.aspects);
            } else {
                res.status(400).send(error.message)
            }
        });
    });

    // Get asset variables
    api.get('/:id/variables', (req, res) => {
        httpRequest.get({
            url: mspAssetURL+req.params.id+'/variables',
            headers: { 'authorization': 'Bearer ' + req.token, 'content-type': 'application/json' }
        }, function (error, answer, body) {
            if (!error && answer.statusCode == 204 || 200 ) {
                res.status(200).json(JSON.parse(body)._embedded.variables);
            } else {
                res.status(400).send(error.message)
            }
        });
    });

    // Update asset details
    api.put('/:id', (req, res) => {
        httpRequest.put({
            url: mspAssetURL+req.params.id,
            headers: { 'authorization': 'Bearer ' + req.token, 'content-type': 'application/json', 'If-Match': req.headers["if-match"]}
        }, function (error, answer, body) {
            if (!error && answer.statusCode == 204 || 200 ) {
                res.status(200).json(JSON.parse(body));
            } else {
                res.status(400).send(error.message)
            }
        });
    });

    // Move asset
    api.post('/:id/move', (req, res) => {
        httpRequest.post({
            url: mspAssetURL+req.params.id+'/move',
            headers: { 'authorization': 'Bearer ' + req.token, 'content-type': 'application/json', 'If-Match': req.headers["if-match"]},
            body: req.body
        }, function (error, answer, body) {
            if (!error && answer.statusCode == 204 || 200 ) {
                res.status(200).json(JSON.parse(body));
            } else {
                res.status(400).send(error.message)
            }
        });
    });

    // Get root asset
    api.get('/root', (req, res) => {
        httpRequest.get({
            url: mspAssetURL+'/root',
            headers: { 'authorization': 'Bearer ' + req.token, 'content-type': 'application/json'},
        }, function (error, answer, body) {
            if (!error && answer.statusCode == 204 || 200 ) {
                res.status(200).json(JSON.parse(body));
            } else {
                res.status(400).send(error.message)
            }
        });
    });

    // Create asset
    api.post('/', (req, res) => {
        httpRequest.post({
            url: mspAssetURL,
            headers: { 'authorization': 'Bearer ' + req.token, 'content-type': 'application/json' },
            body: req.body
        }, function (error, answer, body) {
            if (!error && answer.statusCode == 201) {
                res.status(201).json(JSON.parse(body));
            } else {
                res.status(400).send(error.message)
            }
        });
    });

    // Delete asset
    api.delete('/:id', (req, res) => {
        httpRequest.delete({
            url: mspAssetURL+req.params.id,
            headers: { 'authorization': 'Bearer ' + req.token, 'content-type': 'application/json', 'If-Match': req.headers["if-match"]},
        }, function (error, answer, body) {
            if (!error && answer.statusCode == 204 || 200) {
                res.status(200).json(JSON.parse(body));
            } else {
                res.status(400).send(error.message)
            }
        });
    });

    // Patch asset
    api.post('/:id', (req, res) => {
        httpRequest.post({
            url: mspAssetURL+req.param.id,
            headers: { 'authorization': 'Bearer ' + req.token, 'content-type': 'application/json', 'If-Match': req.headers["if-match"]},
            body: req.body
        }, function (error, answer, body) {
            if (!error && answer.statusCode == 200 || 204) {
                res.status(200).json(JSON.parse(body));
            } else {
                res.status(400).send(error.message)
            }
        });
    });

    // Store static aspect data
    api.put('/:id', (req, res) => {
        httpRequest.put({
            url: mspAssetURL+req.param.id,
            headers: { 'authorization': 'Bearer ' + req.token, 'content-type': 'application/json', 'If-Match': req.headers["if-match"]},
            body: req.body
        }, function (error, answer, body) {
            if (!error && answer.statusCode == 200 || 204) {
                res.status(200).json(JSON.parse(body));
            } else {
                res.status(400).send(error.message)
            }
        });
    });

module.exports = api
