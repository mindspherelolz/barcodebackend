const { Router } = require('express')
const httpRequest = require('request')
const { mspTimeseriesURL,mspAggregateURL } = require('./mspConfig')


let api = Router();

    // '/msp/timeseries/' - Read
    api.get('/:assetId/:assetType', (req, res) => {
        if(!req.token) return res.status(401).send('Acess denied. No token provided.');
        httpRequest.get({
            url: mspTimeseriesURL + `${req.params.assetId}/${req.params.assetType}`,
            headers: { 'authorization': 'Bearer ' + req.token, 'content-type': 'application/json' },
            qs: req.query
        }, function (error, answer, body) {
            if (!error && answer.statusCode == 204 || 200 ) {
                res.status(200).json(JSON.parse(body));
            } else {
                res.status(400).send(error.message)
            }
        });
    });

    // '/msp/aggregate/' - Read
    api.get('/aggregates/:assetId/:assetType', (req, res) => {
        if(!req.token) return res.status(401).send('Acess denied. No token provided.');
        httpRequest.get({
            url: mspAggregateURL + `${req.params.assetId}/${req.params.assetType}?from=${req.query.from}&to=${req.query.to}&intervalValue=${req.query.intervalValue}&intervalUnit=${req.query.intervalUnit}`,
            headers: { 'authorization': 'Bearer ' + req.token, 'content-type': 'application/json' },
            qs: req.query
        }, function (error, answer, body) {
            if (!error && answer.statusCode == 204 || 200 ) {
                res.status(200).json(JSON.parse(body));
            } else {
                res.status(400).send(error.message)
            }
        });
    });


module.exports = api
