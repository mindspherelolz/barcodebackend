const http  = require('http');
const express = require('express')
const socket = require('socket.io');
const bodyParser = require('body-parser')
const config = require('./config')
const routes = require('./routes')
const msp = require('./routes/msp')
const getAdminTokenMid = require('./middleware/getAdminTokenMid')

let app = express()
app.server = http.createServer(app);
//Socket io config
const defaultPath = '/backend';
let io = socket(app.server,{
  path: defaultPath + '/socket',
  origins: '*:*'
});

//CROS
app.use("*", function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header("Access-Control-Allow-Headers", "Content-Type,Content-Length, Authorization, Accept,X-Requested-With");
  res.header("Access-Control-Allow-Methods","PUT,POST,GET,DELETE,OPTIONS,PATCH");
  res.header("Cache-Control", "no-cache");
  // res.set('etag','strong');
  
  if (req.method === 'OPTIONS') {
    res.sendStatus(200)
  } else {
    next()
  }
});

// app.enable('etag');

//middleware
// parse application/json
app.use(bodyParser.json({
    limit: config.bodyLimit
}));

/////////////// Routing ////////////////////////
// api routes v1
app.use(defaultPath + '/v1', routes);

// api MindSphere
app.use(defaultPath + '/msp', getAdminTokenMid, msp);
/////////////// Routing ////////////////////////



/////////////// Testing Use ////////////////////////
app.get(defaultPath, (req, res) => {
  res.json({ message: 'Bar Code API is ALIVE~' });
});
app.get(defaultPath + '/token', getAdminTokenMid, (req, res) => {
  res.json({ token: req.token })
});
/////////////// Testing Use ////////////////////////

/////////////// Socket Import ////////////////////////
require('./socket/BarcodeScanner')(io);
require('./socket/Item')(io);
require('./socket/ItemList')(io);
require('./socket/Record')(io);
require('./socket/User')(io);
require('./socket/Report')(io);
// require('./socket/ScanList')(io);
/////////////// Socket Import ////////////////////////

// Listening  port


module.exports = app
