const ItemList = require('../model/ItemList');
const ScanList = require('../model/ScanList');
const Item = require('../model/Item');
const Record = require('../model/Record');
const User = require('../model/User');

module.exports = (io) => {
    let itemListSocket = io
    itemListSocket.on('connection', function(client){
        console.log('A user connected into item list socket.');

        //Create Item List
        client.on('createItemList', async(itemList) => {
            try{
                const newItemList = await new ItemList(itemList);
                const newScanList = await new ScanList();
                newScanList.itemList = newItemList._id;
                newItemList.scanList = newScanList._id;
                newItemList.items.forEach(item => {
                    newScanList.items.push({
                        _id: item._id,
                        demand: item.quantity
                    });
                });
                await newItemList.save();
                await newScanList.save();
                itemListSocket.emit('itemListCreated',newItemList);
                console.log('new item list created.');
            }catch(e){
                itemListSocket.emit('Error', e);
                console.log(e);
            }
        });

        // Update ItemList
        client.on('updateItemList', async(itemList) => {
            try{
                const updates = Object.keys(itemList)
                const allowedUpdates = config.itemListAllowedUpdates
                const isValidOperation = updates.every((update) => allowedUpdates.includes(update))
                if (!isValidOperation) {
                    console.log({ error: 'Invalid updates!' });
                }else{
                    const newItemList = await ItemList.findOne({ _id: req.params.id })
                    if (!newItemList) {
                        console.log('item list not found.');
                    }else{
                        updates.forEach((update) => newItemList[update] = itemList[update])
                        await newItemList.save(function(err,itemList){
                                if(!err){
                                    console.log('item list updated.');
                                    itemListSocket.emit('itemListUpdated',itemList);
                                }else{
                                    itemListSocket.emit('Error', err);
                                    console.log(err);
                                }
                        });
                    }
                }
            }catch(e){
                itemListSocket.emit('Error', e);
                console.log(e);
            }
        });

        //Delete item list 
        client.on('deleteItemList', async(Id) => {
            try {
                const itemList = await ItemList.findOne({ _id: Id })
                const scanList = await ScanList.findOne({ itemList: Id })
                if (!itemList || !scanList){
                    console.log('item list or scan lsit not found.');
                }else{
                    await scanList.remove();
                    await itemList.remove(function(err,itemList){
                        if(!err){
                            console.log('item list deleted.');
                            itemListSocket.emit('itemListDeleted',itemList);
                        }else{
                            itemListSocket.emit('Error', err);
                            console.log(err);
                        }
                    });
                }
            } catch (e) {
                itemListSocket.emit('Error', e);
                console.log(e);
            }
        });


        //Scan items to an item list 
        client.on('scanItem', async(Id, target) => {
            try{
                const itemList = await ItemList.findOne({ _id: Id });
                const scanList = await ScanList.findOne({ itemList: Id });
                if (!itemList) {
                    console.log('Item list is not found.');
                } else {
                    // Step 1: Get the scanned Items
                    if(target){
                        const item = await Item.findById(target.id);
                        scanedItems = {
                            _id: item._id,
                            scaned: target.quantity,
                            wrong: true
                        }
                        
                        // Save to DB
                        // 'item' is the iterative object in scanList.items
                        // 'scanedItems' is the data sent from clients
                        var found = false;
                        scanList.items.forEach(item => {
                            if(JSON.stringify(item._id) == JSON.stringify(scanedItems._id)){
                                found = true;
                                item.scaned += scanedItems.scaned;
                                if (item.scaned == item.demand) {
                                    item.wrong = false
                                } else {
                                    item.wrong = true
                                }
                            }
                        });

                        if(!found){
                            // This item is not necessary, wrong!!!
                            // Push this scannedItem to scanList, default: wrong = true, demand = 0
                            scanList.items.push(scanedItems);
                        }
                        var unfilled = scanList.items.find(item => {
                            return item.scaned != item.demand;
                        });
                        
                        if(!unfilled){
                            itemList.fulfilled = true;
                        }else{
                            itemList.fulfilled = false;
                        }

                        await scanList.save();
                        await itemList.save(function(err,itemList){
                            if(!err){
                                console.log('an item scanned.');
                                itemListSocket.emit('itemScanned',itemList);
                            }else{
                                console.log(err);
                                itemListSocket.emit('Error', err);
                            }
                        });
                    }else{
                        itemListSocket.emit('Error', 'item not define.');
                        console.log('item not define.');
                    }           
                }
            } catch(e) {
                itemListSocket.emit('Error', e);
                console.log({ error: 'Scan Error' , e: e.message});
            }
        });

        //Submit Item List 
        client.on('submitItemList', async(Id, email) => {
            try{
                const itemList = await ItemList.findOne({ _id: Id});
                const scanList = await ScanList.findOne({ itemList: Id});
                const user = await User.findOne({ email: email });
                if(user){
                    itemList.submitted = true;
                    itemList.operator = user._id;
                    await itemList.save();

                    if(itemList.fulfilled){
                        user.correct.push(itemList._id);
                    }else{
                        user.wrong.push(itemList._id);
                    }
                    await user.save();
                    
                    const newRecord = await new Record();
                    newRecord.user = user._id;
                    newRecord.result = itemList.fulfilled;
                    newRecord.itemList = itemList._id;
                    newRecord.scanList = scanList._id;
                    await newRecord.save(function(err,record){
                        if(!err){
                            console.log('an item list submitted.');
                            itemListSocket.emit('itemListSubmitted',record);
                        }else{
                            itemListSocket.emit('Error', err);
                            console.log(err);
                        }
                    });
                }else {
                    let err = 'User not found.'
                    itemListSocket.emit('Error', err);
                    console.log(err);
                }
             }catch(e){
                itemListSocket.emit('Error', e);
                console.log({ error: 'Submit Error' , e: e.message});
             }
        });

    });
}