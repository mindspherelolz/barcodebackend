const BarcodeScanner = require('../model/BarcodeScanner');
const config = require('../config');
const { getTSData } = require('../utils/routine');

module.exports = (io) => {

    let scannerSocket = io;
    let getTSDataInterval;

    scannerSocket.on('connection', function(client){
        console.log('A user connected into scanner socket.');
       
        getTSDataInterval = setInterval(() => {
            getTSData(client);
        }, config.TS_INTERVAL);

        // Delete all scanned bar code record.
        client.on('deleteAllScanned', async() => {
            try{
                await BarcodeScanner.deleteMany({}, (err, scanned) => {
                    if (err){
                        console.log(err);
                    }else {
                        console.log('Delete all scanned successfully!!');
                        scannerSocket.emit('AllScannedDeleted', scanned);
                    }
                });
            }catch(e){
                console.log(e);
            }
        });

        client.on('disconnect', () => {
            clearInterval(getTSDataInterval);
        })
        
    });
}