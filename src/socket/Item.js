const Item = require('../model/Item');

module.exports = (io) => {
    let itemSocket = io
    itemSocket.on('connection', function(client){
        console.log('A user connected into item socket.');

        // Create an Item 
        client.on('createItem', async(item) => {
            try{
                let newItem = await new Item(item);
                await newItem.save(function(err, item){
                    if(err){
                        console.log(err);
                    }else{
                        console.log('new item created.');
                        itemSocket.emit('itemCreated',item);
                    }
                });
            }catch(e){
                console.log(e);
            }
        });

        // Update an item by id
        client.on('updateItem', async(item) => {
            try{
                const updates = Object.keys(item)
                const allowedUpdates = config.itemAllowedUpdates
                const isValidOperation = updates.every((update) => allowedUpdates.includes(update))
                if (!isValidOperation) {
                    console.log({ error: 'Invalid updates!' });
                }
    
                await Item.findByIdAndUpdate(item._id, item, { new: true }, (err,item)=>{
                    if(err){
                        console.log(err);
                    }else{
                        console.log('item updated.');
                        itemSocket.emit('itemUpdated',item);
                    }
                });
                
            }catch(e){
                console.log(e);
            }
        });

        // Delete an item by id
        client.on('deleteItem', async(Id) => {
            try{
                // const itemList = await ItemList.findOne({ items: req.params.id })
                // itemList['items'].filter((itemId, idx) => {
                //     return itemId == req.params.id
                // })
                // itemList.save()

                const item = await Item.findOne({_id: Id })
                if (!item){
                    console.log('item not found.');
                }else{
                    await item.remove(function(err,item){
                        console.log('item deleted.');
                        itemSocket.emit('itemDeleted',item);
                    });
                }
                

            }catch(e){
                console.log(e);
            }
        });
        

    });
}