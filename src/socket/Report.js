const Report = require('../model/Report');

module.exports = (io) => {
    let reportSocket = io
    reportSocket.on('connection', function(client){
        console.log('A user connected into report socket.');

        // Create an Report 
        client.on('CREATE_REPORT', async(report) => {
            try{
                let newReport = await new Report(report);
                await newReport.save(function(err, report){
                    if(err){
                        console.log(err);
                    }else{
                        console.log('new report created.');
                        reportSocket.emit('REPORT_CREATED',report);
                        if(report.people.includes("管理員")){
                            reportSocket.emit('ADMIN_REPORT_CREATED',report);
                        }else if(report.people.includes("維修員")){
                            reportSocket.emit('MAIN_REPORT_CREATED',report);
                        }else{
                            reportSocket.emit('SITE_REPORT_CREATED',report);
                        }
                    }
                });
            }catch(e){
                console.log(e);
            }
        });

        // Update an report by id
        client.on('UPDATE_REPORT', async(report) => {
            try{
    
                await Report.findByIdAndUpdate(report._id, report, { new: true }, (err,report)=>{
                    if(err){
                        console.log(err);
                    }else{
                        console.log('report updated.');
                        reportSocket.emit('REPORT_UPDATED',report);
                    }
                });
                
            }catch(e){
                console.log(e);
            }
        });

        // Check an report by id
        client.on('CHECK_REPORT', async(Id) => {
            try{
                const report = await Report.findOne({ _id: Id});
                if (!report){
                    console.log('report not found.');
                }else{
                    report.acknowledge = true;
                    await report.save(function(err,report){
                        console.log('report checked.');
                        reportSocket.emit('REPORT_CHECKED',report);
                    });
                }
            }catch(e){
                console.log(e);
            }
        });

        // Delete an report by id
        client.on('DELETE_REPORT', async(Id) => {
            try{

                const report = await Report.findOne({_id: Id })
                if (!report){
                    console.log('report not found.');
                    reportSocket.emit('ERROR','report not found.');
                }else{
                    await report.remove(function(err,report){
                        if(!err){
                            console.log('report deleted.');
                            reportSocket.emit('REPORT_DELETED',report);
                        }else{
                            reportSocket.emit('ERROR',err);
                        }
                    });
                }
                
            }catch(e){
                console.log(e);
            }
        });
        

    });
}