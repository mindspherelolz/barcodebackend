const Record = require('../model/Record');

module.exports = (io) => {
    let recordSocket = io
    recordSocket.on('connection', function(client){
        console.log('A user connected into record socket.');

        // // Delete all Records
        client.on('deleteAllRecord', async() => {
            try {
                await Record.deleteMany(function(err,records){
                    if(!err){
                        console.log('all records deleted.');
                        recordSocket.emit('allRecordDeleted',records);
                    }else{
                        console.log(err);
                    }
                })
            } catch (e) {
                console.log(e);
            }
        });

        //Delete record
        client.on('deleteRecord', async(Id) => {
            try {
                const record = await Record.findOne({ _id: Id })
                if (!record){
                    console.log('record not found.');
                }else{
                    await record.remove(function(err,record){
                        if(!err){
                            console.log('record deleted.');
                            recordSocket.emit('recordDeleted',record);
                        }else{
                            recordSocket.emit('Error', err);
                            console.log(err);
                        }
                    });
                }
            } catch (e) {
                recordSocket.emit('Error', e);
                console.log(e);
            }
        });
    });
}