const User = require('../model/User');

module.exports = (io) => {
    let userSocket = io
    userSocket.on('connection', function(client){
        console.log('A user connected into user socket.');

        // Create an User 
        client.on('createUser', async(user) => {
            try{
                let newUser = await new User(user);
                await newUser.save(function(err, user){
                    if(err){
                        console.log(err);
                    }else{
                        console.log('new user created.');
                        userSocket.emit('userCreated',user);
                    }
                });
            }catch(e){
                console.log(e);
            }
        });

        // Update an user by id
        client.on('updateUser', async(user) => {
            try{
                const updates = Object.keys(user)
                const allowedUpdates = config.userAllowedUpdates
                const isValidOperation = updates.every((update) => allowedUpdates.includes(update))
                if (!isValidOperation) {
                    return res.status(400).send({ error: 'Invalid updates!' })
                }
    
                await user.findByIdAndUpdate(user._id, user, { new: true }, (err,user)=>{
                    if(err){
                        console.log(err);
                    }else{
                        console.log('user updated.');
                        userSocket.emit('userUpdated',user);
                    }
                });
                
            }catch(e){
                console.log(e);
            }
        });

        // Delete an item by id
        client.on('deleteUser', async(Id) => {
            try{
                const user = await User.findOne({_id: Id })
                if (!user){
                    console.log('user not found.');
                }else{
                    await user.remove(function(err,user){
                        console.log('user deleted.');
                        userSocket.emit('userDeleted',user);
                    });
                }
                

            }catch(e){
                console.log(e);
            }
        });
        

    });
}