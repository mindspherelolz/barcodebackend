
const checkAdmin = async (req, res, next) => {
    try {
        if (!req.user.isAdmin) {
            //res.status(403).send('You are not allowed.')
        }
        next()
    } catch (e) {
        res.status(400).send({ error: e.message })
    }
}

module.exports = checkAdmin