const jwtDecode = require('jwt-decode')
const User = require('../model/User')

const getUser = async (req, res, next) => {
    try {

        const decoded = jwtDecode(req.token);
        const name = decoded["user_name"];
        const email = decoded["email"];

        // Create users during first login
        req.user = await User.findOne({ email });
        if (!req.user) {
            const newUser = new User({ name: name, email: email });
            await newUser.save()
            req.user = newUser
        }

        next()
    } catch (e) {
        // res.status(401).send({ error: 'Please authenticate.' , e})
        res.status(401).send({ error: e.message, user: req.user })

    }
}

module.exports = getUser