const jwtDecode = require('jwt-decode')

const getToken = (req, res, next) => {
    try {
        const token = req.header('Authorization').replace('Bearer ', '')
        req.token = token

        // Decode the token to get the user_name
        const decoded = jwtDecode(token)
        req.user_name = decoded["user_name"]

        next()
    } catch (e) {
        res.status(401).send({ error: 'Please authenticate.' , e})
    }
}

module.exports = getToken