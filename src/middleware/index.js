const { Router } = require('express')
const getTokenMid = require('./getTokenMid')
const getUser = require('./getUser')


module.exports = ({ config, db }) => {
    let api = Router();
    //add middleware

    api.use(getTokenMid)
    api.use(getUser)


    return api;
}