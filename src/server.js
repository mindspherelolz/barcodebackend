const app = require('./app')
const config = require('./config')

app.server.listen(config.port);

console.log(`Started on port ${app.server.address().port}`);


  