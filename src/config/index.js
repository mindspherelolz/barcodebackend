let d = new Date();
// d.getHours(d.setMinutes - 2);
module.exports = {
	"port": process.env.PORT,
	// "port": 3000,
	// "mongoUrl": "mongodb://localhost:27017/BarcodeBackend",
	// "mongoUrl": process.env.MONGODB_URL,
	// "mongoUrl":"mongodb://NickSeimens:seimens01@ds125602.mlab.com:25602/seimens-pratice",
	"mongoUrl":"mongodb://a9s-brk-usr-630da429abbe0525b3e4f89c485d18d4ded579cf:a9s2036ee01869aeee52abb6f611dae7c13a965b26d@mod60ba2d-mongod-initial-master-0.node.dc1.a9ssvc:27017/mod60ba2d",
	"bodyLimit": "100kb", 
	"userAllowedUpdates": ['name', 'email', 'cardId', 'isAdmin'],
	"itemListAllowedUpdates": ['name', 'operator','production'],
	"itemAllowedUpdates": ['name'],

	"TS_INTERVAL": 1000,
	"barcodeScannerAssetID": ["45342553b519480f8b8579d605b2bc3b"],
	"START_TIME": d.toISOString()

}