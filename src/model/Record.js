const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const recordSchema = new Schema({
    // The employee who submit this verification
    user: {
        type: Schema.ObjectId,
        ref: 'User'
    },
    itemList: {
        type: Schema.ObjectId,
        required: true,
        ref: 'ItemList'
    },
    scanList: {
        type: Schema.ObjectId,
        required: true,
        ref: 'ScanList'
    },
    // If the 'fulfilled' in itemList if true
    result: {
        type: Boolean,
        required: true,
        default: false
    }
}, {
    timestamps: true
});


module.exports = mongoose.model('Record', recordSchema);
