const mongoose = require('mongoose')
const Schema = mongoose.Schema;

// This model is used to store data that are collected from MSP.
// Each data point means a BarcodeScanner(assetID) scanned an Item (scannedValue) at (time).

const barcodeScannerSchema = new Schema({
    time: { 
        type: String,
        required: true
    },
    scannedValue: { // specify which item
        type: Schema.Types.ObjectId,
        ref: 'Item',
        required: true,
    },
    assetID: {
        type: String,
        required: true
    },
    belong: {
        type: Schema.Types.ObjectId,
        ref: 'ItemList'
    }

});

barcodeScannerSchema.statics.getLatestTime = async function(assetID) {
    const all = await this.find({assetID})
    if (!all) return null
    let latestTime = null
    all.forEach(datapoint => {
        if (!latestTime) {
            latestTime = datapoint.time
        } else {
            if (latestTime < datapoint.time) {
                latestTime = datapoint.time
            }
        }
    });
    return latestTime
}

barcodeScannerSchema.statics.getLatestItemList = async function(assetID) {
    const all = await this.find({assetID})
    if (!all) return null
    let latestTime = null
    let ans = null
    all.forEach(datapoint => {
        if (!latestTime) {
            latestTime = datapoint.time
            ans = datapoint.belong
        } else {
            if (latestTime < datapoint.time) {
                latestTime = datapoint.time
                ans = datapoint.belong
            }
        }
    });
    return ans
}

module.exports = mongoose.model('BarcodeScanner', barcodeScannerSchema);
