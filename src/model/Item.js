const mongoose = require('mongoose')
const Schema = mongoose.Schema;
const ItemList = require('./ItemList')

const itemSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    category: {
        type: String,
        required: true,
        default: 'Food'
    },
    quantity: {
        type: Number,
        default: 0,
        require: true
    }
}, {
    timestamps: true
});

// userSchema.plugin(passportLocalMongoose);
module.exports = mongoose.model('Item', itemSchema);
