const mongoose = require('mongoose')
const Schema = mongoose.Schema;
const validator = require('validator')

const userSchema = new Schema({
    name: {
        type: String,
        required: true
        // unique: true,
        // sparse: true
    },
    email: {
        type: String,
        required: true,
        unique: true,
        lowercase: true,
        trim: true,

        validate(value) {
            if (!validator.isEmail(value)) {
                throw new Error('Email is invalid.')
            }
        }
    },
    department: String, default: "",
    phone: String, default: "",
    isAdmin: {
        type:Boolean, 
        default: true
    },
    wrong: {
        type: Array,
        required: true,
        default: []
    },
    correct: {
        type: Array,
        required: true,
        default: []
    }
}, {
    timestamps: true
});

const User = mongoose.model('User', userSchema);
module.exports = User
