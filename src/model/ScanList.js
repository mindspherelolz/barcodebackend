const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const Item = require('./Item');
const ItemList = require('./ItemList');

const scanListSchema = new Schema({
    itemList: {
        type: Schema.Types.ObjectId,
        required: true,
        ref: 'ItemList'
    },
    items: [{
        item: {
            type: Schema.Types.ObjectId,
            ref: 'Item'
        },
        demand: {
            type: Number,
            require: false,
            default: 0
        },
        scaned: {
            type: Number,
            required: true,
            default: 0
        },
        wrong: {
            type: Boolean,
            required: false,
            default: true
        }
    }]
}, {
    timestamps: true
});

// userSchema.plugin(passportLocalMongoose);
module.exports = mongoose.model('ScanList', scanListSchema);