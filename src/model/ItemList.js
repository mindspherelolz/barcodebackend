const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const Item = require('./Item');
const ScanList = require('./ScanList')

// Definition:
//  - name:
//      The name of this ItemList. Ex: '測試訂單'
//  - items:
//      Specify the required items of this ItemList, including their item id and quantity.
//  - scanList: 
//      Every ItemList has a corresponding ScanList, which stores the scanning result.
//  - fulfilled:
//      Only when the scanning result is 'exactly correct', this will be true.
//  - submitted:
//      The ItemList can be submitted if it is fulfilled. Once it is submitted, it will be recorded in Record,
//      and can not be modified.
//
const itemListSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    items: [{
        item: {
            type: Schema.Types.ObjectId,
            ref: 'Item'
        },
        quantity: {
            type: Number,
            required: true,
            default: 1
        }
    }],
    scanList: {
        type: Schema.Types.ObjectId,
        ref: 'ScanList'
    },
    production: {
        type: String,
        required: true,
        default: 'Unknown'
    },
    operator: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    fulfilled: {
        type: Boolean,
        require: true,
        default: false
    },
    submitted: {
        type: Boolean,
        require: true,
        default: false
    }
}, {
    timestamps: true
});


// userSchema.plugin(passportLocalMongoose);
module.exports = mongoose.model('ItemList', itemListSchema);
