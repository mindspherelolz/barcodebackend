const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const reportSchema = new Schema({
    title: {
        type: String,
        required: true
    },
    production: {
        type: String,
        required: true
    },
    date: {
        type: String,
        required: true
    },
    time: {
        type: String,
        required: true
    },
    emergency: {
        type: Boolean,
        required: true,
        default: false
    },
    category: {
        type: String,
        required: true
    },
    people: {
        type: Array,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    acknowledge: {
        type: Boolean,
        required: true,
        default: false
    }
}, {
    timestamps: true
});

// userSchema.plugin(passportLocalMongoose);
module.exports = mongoose.model('Report', reportSchema);