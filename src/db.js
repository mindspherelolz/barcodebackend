const mongoose = require('mongoose')
const config = require('./config')

module.exports = callback => {
  let db;
  // Connect to the database before starting the application server.
  mongoose.connect(config.mongoUrl, { useNewUrlParser: true, useCreateIndex: true,  useUnifiedTopology: true }, function (err, database) {
    if (err) {
      console.log(err);
      process.exit(1);
    }
    // console.log(config.mongoUrl);
    // Save database object from the callback for reuse.
    db = database;
    // console.log("Database connection ready");
    callback(db);
  });
}
