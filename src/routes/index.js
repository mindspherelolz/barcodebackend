const express = require('express')
const config = require('../config')
const initalizeDb = require('../db')
const user = require('../controller/user')
const itemList = require('../controller/itemList')
const scanList = require('../controller/scanList')
const item = require('../controller/item')
const record = require('../controller/record')
const report = require('../controller/report')

const barcodeScanner = require('../controller/barcodeScanner')

const { getTSData } = require('../utils/routine')

const router = express();

//connect to db
initalizeDb(db => {

  //internal middleware
  //router.use(middleware({ config, db }));

  //api routes v1 (/v1)
  router.use('/user', user({ config, db }));

  router.use('/itemlist', itemList({ config, db }))
  router.use('/item', item({ config, db }))
  router.use('/record', record({ config, db }))
  router.use('/barcodeScanner', barcodeScanner({ config, db }))
  router.use('/scanlist', scanList({ config, db }));
  router.use('/report', report({ config, db }));

  // setInterval(getTSData, config.TS_INTERVAL);
});

module.exports = router;
