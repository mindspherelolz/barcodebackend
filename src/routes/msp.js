const express = require('express')
const token = require('../controller/msp/token')
const events = require('../controller/msp/events')
const assets = require('../controller/msp/assets')
const eventTypes = require('../controller/msp/eventTypes')
const agents = require('../controller/msp/agents')
const assetTypes = require('../controller/msp/assetTypes')
const timeseries = require('../controller/msp/timeseries')
const file = require('../controller/msp/file')
const all = require('../controller/msp/all')
const getTokenMid = require('../middleware/getTokenMid');


const router = express();
//MindSphere api

// /backend/msp/PATH
router.use('/token', getTokenMid, token);
router.use('/assets', assets);
router.use('/assetTypes', assetTypes);
router.use('/events', events);
router.use('/eventTypes',eventTypes);
router.use('/agents', agents);
router.use('/timeseries', timeseries);
router.use('/file', file);

// This simply send the request to mindsphere api service.
router.use('/original', all);

module.exports = router;
