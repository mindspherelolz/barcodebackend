const btoa = require('btoa');
const axios = require('axios');

const getToken = async () =>  {

    try{
        var clientId = "sagtwdev-vuetest-1.0.0" || 'localdev'
        var clientSecret = "kVyzMHvQsHZRfVCw2MtEcqvwX7rWh7HN9yPNeTeT7VS" || 'localdev';
        var appName = "vuetest" || 'localdev';
        var appVersion = "1.0.0" || 'localdev';
        var stringToEncode = clientId + ':' + clientSecret;
        var base64 = btoa(stringToEncode);
    
        var jsondata = {
            appName: appName,
            appVersion: appVersion,
            hostTenant: 'sagtwdev',
            userTenant: 'sagtwdev'
        };
        let res = await axios.post('https://gateway.eu1.mindsphere.io/api/technicaltokenmanager/v3/oauth/token',jsondata,{headers: { 'X-SPACE-AUTH-KEY': 'Basic ' + base64, 'content-type': 'application/json' }});
        return res.data.access_token;
    }catch(e){
        console.log('Get third party token Error:' + e);
    }
}


module.exports = {
    getToken
}