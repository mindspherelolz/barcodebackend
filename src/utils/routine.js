const httpRequest = require('request')
const BarcodeScanner = require('../model/BarcodeScanner')
const config = require('../config')
const ScanList = require('../model/ScanList')
const ItemList = require('../model/ItemList')
const {getToken} = require('./utils')



const getTSData = async (client)  => {
    // TOKEN = 'eyJhbGciOiJSUzI1NiIsImprdSI6Imh0dHBzOi8vc2FndHdkZXYubG9jYWxob3N0OjgwODAvdWFhL3Rva2VuX2tleXMiLCJraWQiOiJrZXktaWQtMiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiIwZmI3Y2VkN2YzMWY0NDA5OWZjZjM2YjE1MGJhNWRkYSIsInN1YiI6InNhZ3R3ZGV2LXNtYXJ0YnVpbGRpbmctMS4wLjA1NiIsInNjb3BlIjpbIm1kc3A6Y29yZTpBZG1pbjNyZFBhcnR5VGVjaFVzZXIiXSwiY2xpZW50X2lkIjoic2FndHdkZXYtc21hcnRidWlsZGluZy0xLjAuMDU2IiwiY2lkIjoic2FndHdkZXYtc21hcnRidWlsZGluZy0xLjAuMDU2IiwiYXpwIjoic2FndHdkZXYtc21hcnRidWlsZGluZy0xLjAuMDU2IiwiZ3JhbnRfdHlwZSI6ImNsaWVudF9jcmVkZW50aWFscyIsInJldl9zaWciOiI0ZDJjMjg2OSIsImlhdCI6MTU3NDg2NzI4NywiZXhwIjoxNTc0ODY5MDg3LCJpc3MiOiJodHRwczovL3NhZ3R3ZGV2LnBpYW0uZXUxLm1pbmRzcGhlcmUuaW8vb2F1dGgvdG9rZW4iLCJ6aWQiOiJzYWd0d2RldiIsImF1ZCI6WyJzYWd0d2Rldi1zbWFydGJ1aWxkaW5nLTEuMC4wNTYiXSwidGVuIjoic2FndHdkZXYiLCJzY2hlbWFzIjpbInVybjpzaWVtZW5zOm1pbmRzcGhlcmU6aWFtOnYxIl0sImNhdCI6ImNsaWVudC10b2tlbjp2MSJ9.pD3Yxx5p66GJFoziHJ5LOmSvUcLoKTPy3WsDVx4ZvZaz5d4Akmav1uOa2CO_NGZW6wgYpSh8x3TJ8XSY2Ro0u6LpnxwSM36OTnZtFvX_2_ayJ2YsjawZJzRLHsO7WnBdA-iqowOqXJS0fwvVfP8txNKhBJ6Z8vpm9iUE2WUk958jQpU_tNmNn6RzWC18aDVZQqaTxpqnSbvgXDSFuTP3Oirq1GA8AG4rzuVvRyUefRZm2wEaMzPconSzFlxFZN0TmseLOUNOcLt9-5imIIeJdPxbzmKmu9hYARhAJfYAKAnQhdeAAFu9B2PT_J3aYiiV750XjPgAd6tKql4IZAREXQ'
    // TOKEN = req.token
    barcodeScannerAssetIDs = config.barcodeScannerAssetID
    try {
        // Get data points from each scanner
        barcodeScannerAssetIDs.forEach(async assetID => {

            // Get data points of one scanner via iot api
            let from = await BarcodeScanner.getLatestTime(assetID)
            if (!from) {
                from = config.START_TIME
            }
            const d = new Date()
            d.setMonth(d.getMonth() + 1);
            let to = d.toISOString();
            // let from = "2019-11-06T00:00:00Z"
            // let to = "2019-11-13T19:55:00Z"
            console.log("From: ", from)
            TOKEN = await getToken();
            if(TOKEN){
                httpRequest.get({
                    url: 'https://gateway.eu1.mindsphere.io/api/iottimeseries/v3/timeseries/'+ assetID +'/scannedValue/',
                    headers: { 'authorization': 'Bearer ' + TOKEN, 'content-type': 'application/json'},
                    qs: {
                        from: from,
                        to: to
                    }
                }, async function (error, answer, body) {
                    if (!error && [200,201,204].includes(answer.statusCode)) {

                        // Deal with each data point
                        body = JSON.parse(body)
                        console.log(body)
                        for (var k = 0; k < body.length; k++) {
                            data = body[k]
                            itemListID = data.scannedValue.split(":")[0]
                            itemID = data.scannedValue.split(':')[1]

                            new BarcodeScanner({
                                time: data._time,
                                scannedValue: itemID,
                                assetID: assetID,
                                belong: itemListID
                            }).save()

                            const itemList = await ItemList.findById(itemListID)
                            const scanList = await ScanList.findOne({
                                itemList: itemListID
                            })


                            if (!scanList || !itemList) {
                                console.log("Cannot find the list!")
                            } else {

                                // Create an intance of this 'scan'
                                scannedItem = {
                                    _id: itemID,
                                    scaned: 1,
                                    wrong: true
                                }
        

                                // Find if the item is scanned for the first time or not.
                                var found = false
                                var index = -1
                                for (var i = 0; i < scanList.items.length; i++) {
                                    if (JSON.stringify(scanList.items[i]._id) == JSON.stringify(scannedItem._id)) {
                                        index = i;
                                        break
                                    }
                                }

                                if (index != -1) {
                                    // Not first time:
                                    // Add to the existing scanned item, and check if the quantity surpass the demand.
                                    const target = scanList.items[index]
                                    target.scaned += scannedItem.scaned
                                    if (target.demand > 0) {
                                        scannedItem.wrong = false
                                        if (target.scaned > target.demand) {
                                            scannedItem.wrong = true
                                        }
                                    }

                                    target.wrong = scannedItem.wrong

                                    console.log("=================")
                                    console.log("Scanned it!!")
                                    console.log("ItemList:", itemList._id)
                                    console.log("Item:", scannedItem._id, target.scaned)
                                    console.log("Time:", data._time)
                                    console.log("=================")
                                }
                                else {
                                    // First time:
                                    scanList.items.push(scannedItem)
                                }
        
                                var unfilled = scanList.items.find(item => {
                                    return item.scaned < item.demand
                                })
                                if(!unfilled) {
                                    itemList.fulfilled = true
                                }
        
                                await scanList.save()
                                await itemList.save(function(err,itemList){
                                    if(!err){
                                        console.log('an item scanned.');
                                        client.emit('itemScanned',itemList);
                                    }else{
                                        console.log('itemList save: ' + err);
                                    }
                                });
                            }
                        } 

                    } else {
                        console.log('TimeSerise get Error:' + answer.statusCode);
                        console.log(body);
                    }
                });
            }else{
                console.log("Token not found.");
            }
        });
        console.log("Collect MSP data.");
        return client;
    } catch (e) {
        console.log(e)
    }

}

module.exports = {
    getTSData,
}